package io.gridscale.s3example;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.CanonicalGrantee;
import com.amazonaws.services.s3.model.Permission;

public class Change_ACL_S3example {

    public static void main(String[] args) {

        // bucket name
        String bucketName = "karljr";

        //keyName = file you want to change access rights
        String keyName = "karl.gz";

        // S3 Endpoint for gridscale
        String endpoint = "https://gos3.io";

        String accessKey = System.getenv("GRIDSCALE_S3_ACCESS_KEY");
        String secret = System.getenv("GRIDSCALE_S3_SECRET");

        try {
            BasicAWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secret);

            ClientConfiguration clientConfiguration = new ClientConfiguration();
            clientConfiguration.setSignerOverride("S3SignerType");

            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                    .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint,
                            Regions.DEFAULT_REGION.getName()))
                    .withClientConfiguration(clientConfiguration)
                    .build();

            // Get the existing object ACL that we want to modify.
            AccessControlList acl = s3Client.getObjectAcl(bucketName, keyName);

            // Clear the existing list of grants.
            acl.getGrantsAsList().clear();

            // Grant a sample set of permissions, using the existing ACL owner for Full Control permissions.
            acl.grantPermission(new CanonicalGrantee(acl.getOwner().getId()), Permission.Write);
            // acl.grantPermission(new EmailAddressGrantee(emailGrantee), Permission.WriteAcp);

            // Save the modified ACL back to the object.
            s3Client.setObjectAcl(bucketName, keyName, acl);
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but gridscale couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
        } catch (SdkClientException e) {
            // gridscale endpoint couldn't be contacted for a response, or the client
            // couldn't parse the response from gridscale endpoit.
            e.printStackTrace();
        }
    }
}
