package io.gridscale.s3example;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectRequest;

public class Delete_From_s3example {

    public static void main(String[] args) {

        // bucket name
        String bucketName = "testBucket";

        //keyName = file you want to delete
        String keyName = "s3test";

        // S3 Endpoint for gridscale
        String endpoint = "https://gos3.io";

        String accessKey = System.getenv("GRIDSCALE_S3_ACCESS_KEY");
        String secret = System.getenv("GRIDSCALE_S3_SECRET");

        try {
            BasicAWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secret);

            ClientConfiguration clientConfiguration = new ClientConfiguration();
            clientConfiguration.setSignerOverride("S3SignerType");

            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                    .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint,
                            Regions.DEFAULT_REGION.getName()))
                    .withClientConfiguration(clientConfiguration)
                    .build();

            s3Client.deleteObject(new DeleteObjectRequest(bucketName, keyName));
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }
    }
}

